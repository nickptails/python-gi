FROM python:3.10-alpine3.15 AS builder

RUN apk add --no-cache \
        build-base \
        cairo-dev \
        gobject-introspection-dev \
        glib-dev \
        libffi-dev \
        pkgconf && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel \
        pycairo==1.21.0 && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir \
        pycairo==1.21.0 \
        PyGObject==3.42.0


FROM python:3.10-alpine3.15

COPY --from=builder /wheelhouse /wheelhouse
RUN apk add --no-cache \
        cairo \
        cairo-gobject \
        glib \
        gobject-introspection \
        libffi && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir \
        pycairo==1.21.0 \
        PyGObject==3.42.0
